package eu.dragoncodes.dragonlut

import eu.dragoncodes.dragonlut.di.DaggerMainComponent
import eu.dragoncodes.dragonlut.redux.MainAction
import org.junit.Test

class MainTestSuite {

    @Test
    fun testInternetConnectionChanges() {
        val store = DaggerMainComponent.builder().build().buildMainStore()

        store.dispatch(MainAction.InternetConnectionChanged(hasConnection = false))

        assert(!store.state.hasConnection)
    }
}