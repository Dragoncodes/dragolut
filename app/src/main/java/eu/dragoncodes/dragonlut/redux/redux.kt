package eu.dragoncodes.dragonlut.redux

import android.content.Context
import android.net.ConnectivityManager
import eu.dragoncodes.redux.base.Action
import eu.dragoncodes.redux.base.Reducer
import eu.dragoncodes.redux.base.State

data class MainState(val hasConnection: Boolean) : State

class MainStoreReducer : Reducer<MainState> {
    override fun invoke(action: Action, old: MainState): MainState =
        when (action) {

            is MainAction.Sync -> {

                val applicationContext = action.context

                val connectivityManager = applicationContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

                val activeNetworkInfo = connectivityManager.activeNetworkInfo

                invoke(
                    MainAction.InternetConnectionChanged(
                        activeNetworkInfo?.isConnected ?: false
                    ),
                    old
                )
            }

            is MainAction.InternetConnectionChanged -> {
                old.copy(
                    hasConnection = action.hasConnection
                )
            }
            else -> old
        }
}

sealed class MainAction : Action {

    class Sync(val context: Context) : MainAction()

    class InternetConnectionChanged(val hasConnection: Boolean) : MainAction()
}