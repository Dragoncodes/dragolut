package eu.dragoncodes.dragonlut

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import eu.dragoncodes.dragonlut.di.DaggerMainComponent
import eu.dragoncodes.dragonlut.redux.MainAction
import eu.dragoncodes.dragonlut.redux.MainState
import eu.dragoncodes.dragonlut.utils.broadcastReceiver
import eu.dragoncodes.rates.localization.localized
import eu.dragoncodes.rates.utils.dp
import eu.dragoncodes.rates.view.RatesViewController
import eu.dragoncodes.redux.base.Store
import eu.dragoncodes.redux.base.StoreSubscriber
import java.util.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private lateinit var noConnectionTextView: TextView

    @Inject
    lateinit var store: Store<MainState>

    private val storeSubscriber: StoreSubscriber<MainState>

    private val connectionBroadcastReceiver: BroadcastReceiver by lazy {
        broadcastReceiver { context, _ ->

            context ?: return@broadcastReceiver

            store.dispatch(MainAction.Sync(context))
        }
    }

    init {
        DaggerMainComponent.builder().build().inject(this)

        storeSubscriber = object : StoreSubscriber<MainState> {
            override fun newState(state: MainState) {
                handleConnectivity(state.hasConnection)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {

        requestWindowFeature(Window.FEATURE_NO_TITLE)

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val resources = resources
        val configuration = resources.configuration
        configuration.locale = Locale.ENGLISH
        resources.updateConfiguration(configuration, resources.displayMetrics)

        setActionBar(null)

        noConnectionTextView = findViewById(R.id.tvNoConnection)
        noConnectionTextView.text = "Errors.connection".localized()

        findViewById<ViewGroup>(R.id.flCurrencies).addView(RatesViewController(this))
    }

    override fun onStart() {
        super.onStart()

        store.add(storeSubscriber)

        applicationContext.registerReceiver(
            connectionBroadcastReceiver,
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )

        store.dispatch(MainAction.Sync(this))
    }

    override fun onStop() {

        store.remove(storeSubscriber)

        applicationContext.unregisterReceiver(connectionBroadcastReceiver)

        super.onStop()
    }

    private fun handleConnectivity(hasConnection: Boolean) {

        val lp = noConnectionTextView.layoutParams as ViewGroup.MarginLayoutParams

        val start = lp.topMargin
        val end = if (hasConnection) (-30).dp.toInt() else 0

        val animator = ValueAnimator.ofInt(start, end)

        animator.addUpdateListener {
            lp.topMargin = it.animatedValue as Int

            noConnectionTextView.requestLayout()
        }

        animator.start()
    }
}
