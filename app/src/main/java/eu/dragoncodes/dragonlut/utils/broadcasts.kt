package eu.dragoncodes.dragonlut.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

inline fun broadcastReceiver(crossinline receiver: (Context?, Intent?) -> Unit) =
    object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            receiver(context, intent)
        }
    }