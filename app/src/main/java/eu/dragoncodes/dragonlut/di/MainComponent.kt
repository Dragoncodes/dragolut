package eu.dragoncodes.dragonlut.di

import dagger.Component
import eu.dragoncodes.dragonlut.MainActivity
import eu.dragoncodes.dragonlut.redux.MainState
import eu.dragoncodes.redux.base.Store

@Component(modules = [MainModule::class])
interface MainComponent {

    fun buildMainStore(): Store<MainState>

    fun inject(activity: MainActivity)

}