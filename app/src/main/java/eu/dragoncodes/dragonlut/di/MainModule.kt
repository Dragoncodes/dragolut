package eu.dragoncodes.dragonlut.di

import dagger.Module
import dagger.Provides
import eu.dragoncodes.dragonlut.redux.MainState
import eu.dragoncodes.dragonlut.redux.MainStoreReducer
import eu.dragoncodes.redux.base.Store

@Module
class MainModule {

    @Provides
    fun provideMainReducer() = MainStoreReducer()

    @Provides
    fun provideInitialMainState() = MainState(hasConnection = true)

    @Provides
    fun provideMainStore(
        reducer: MainStoreReducer,
        initialState: MainState
    ) = Store(reducer, initialState = initialState)
}