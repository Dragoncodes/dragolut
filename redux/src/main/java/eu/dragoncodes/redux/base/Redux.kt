package eu.dragoncodes.redux.base

interface Reducer<S> {
    fun invoke(action: Action, old: S): S
}

interface Middleware<S : State> {
    fun invoke(store: Store<S>, action: Action, next: DispatchFunction)
}

typealias DispatchFunction = (Action) -> Unit

interface Action

interface State

class Store<S : State>(
    private val reducer: Reducer<S>,
    private val middlewareList: List<Middleware<S>> = emptyList(),
    initialState: S
) {
    private val subscribers = mutableSetOf<StoreSubscriber<S>>()

    var state: S = initialState
        private set(value) {
            field = value
            subscribers.forEach { it.newState(value) }
        }

    fun dispatch(action: Action) {

        middlewareList.forEach { it.invoke(this, action, ::dispatch) }

        state = reducer.invoke(action, state)
    }

    fun add(subscriber: StoreSubscriber<S>) = subscribers.add(element = subscriber)

    fun remove(subscriber: StoreSubscriber<S>) = subscribers.remove(element = subscriber)
}

interface StoreSubscriber<S> {
    fun newState(state: S)
}