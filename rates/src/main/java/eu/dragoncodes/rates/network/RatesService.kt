package eu.dragoncodes.rates.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesService {

    @GET("/latest")
    fun get(@Query("base") currency: String): Call<RateDTO>
}