package eu.dragoncodes.rates.network

import com.google.gson.annotations.SerializedName
import java.util.*

data class RateDTO(

    @SerializedName("base")
    val base: String,

    @SerializedName("date")
    val date: String,

    @SerializedName("rates")
    val rates: HashMap<String, Double>
)