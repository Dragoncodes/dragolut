package eu.dragoncodes.rates.network

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import eu.dragoncodes.rates.data.toRate
import javax.inject.Inject

class RatesProvider @Inject constructor() {

    @Inject
    internal lateinit var service: RatesService

    suspend fun fetch(base: String) = withContext(Dispatchers.IO) {

        val res = service.get(base).execute()

        res.body()?.toRate()
    }
}