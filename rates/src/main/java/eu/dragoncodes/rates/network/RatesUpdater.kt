package eu.dragoncodes.rates.network

import eu.dragoncodes.rates.data.Currency
import eu.dragoncodes.rates.data.Rate
import kotlinx.coroutines.*
import javax.inject.Inject

class RatesUpdater @Inject constructor(
    private val ratesProvider: RatesProvider,
    private val delayAmount: Long,
    private val currencyProvider: () -> Currency
) {

    var job: Job? = null

    fun start(scope: CoroutineScope, updateCallback: (Rate) -> Unit) {

        job = scope.launch {

            while (isActive) {

                delay(delayAmount)

                val rate = ratesProvider.fetch(currencyProvider.invoke()) ?: continue

                updateCallback(rate)
            }
        }
    }

    fun stop() {
        job?.cancel()
    }
}