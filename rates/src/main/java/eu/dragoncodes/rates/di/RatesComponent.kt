package eu.dragoncodes.rates.di

import dagger.Component
import eu.dragoncodes.http.HttpModule
import eu.dragoncodes.rates.network.RatesProvider
import eu.dragoncodes.rates.network.RatesService
import eu.dragoncodes.rates.network.RatesUpdater
import eu.dragoncodes.rates.redux.RatesExchangeState
import eu.dragoncodes.rates.view.RatesViewController
import eu.dragoncodes.redux.base.Store
import javax.inject.Singleton

@Singleton
@Component(modules = [HttpModule::class, RatesModule::class, CurrenciesModule::class])
interface RatesComponent {

    fun buildService(): RatesService

    fun buildRatesProvider(): RatesProvider

    fun buildRatesStore(): Store<RatesExchangeState>

    fun buildRatesUpdater(): RatesUpdater

    fun inject(ratesViewController: RatesViewController)
}