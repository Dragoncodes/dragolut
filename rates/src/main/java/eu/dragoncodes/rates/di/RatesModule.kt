package eu.dragoncodes.rates.di

import dagger.Module
import dagger.Provides
import eu.dragoncodes.rates.data.Currency
import eu.dragoncodes.rates.network.RatesProvider
import eu.dragoncodes.rates.network.RatesService
import eu.dragoncodes.rates.network.RatesUpdater
import eu.dragoncodes.rates.redux.RatesDownloadMiddleware
import eu.dragoncodes.rates.redux.RatesExchangeState
import eu.dragoncodes.rates.redux.RatesReducer
import eu.dragoncodes.redux.base.Store
import kotlinx.coroutines.GlobalScope
import retrofit2.Retrofit

@Module
class RatesModule {

    @Provides
    internal fun providesRatesService(retrofit: Retrofit) =
        retrofit.create(RatesService::class.java)

    @Provides
    internal fun provideRateReducer() = RatesReducer()

    @Provides
    internal fun provideRatesMiddleware(
        ratesProvider: RatesProvider
    ) = listOf(RatesDownloadMiddleware(ratesProvider, GlobalScope))

    @Provides
    internal fun provideInitialState() = RatesExchangeState.Initial

    @Provides
    internal fun provideRateStore(
        reducer: RatesReducer,
        middleware: List<RatesDownloadMiddleware>,
        initialState: RatesExchangeState
    ) = Store(reducer, middleware, initialState)

    @Provides
    internal fun provideDelayTimer() = 1000L

    @Provides
    internal fun provideRatesUpdater(
        ratesProvider: RatesProvider,
        delayAmount: Long,
        currencyProvider: () -> Currency
    ) = RatesUpdater(ratesProvider, delayAmount, currencyProvider)
}