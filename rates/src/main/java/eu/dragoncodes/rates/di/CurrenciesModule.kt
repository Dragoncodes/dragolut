package eu.dragoncodes.rates.di

import dagger.Module
import dagger.Provides
import eu.dragoncodes.rates.data.Currency

@Module
class CurrenciesModule(
    private val currencyProvider: () -> Currency
) {

    @Provides
    fun provideCurrencyProvider(): () -> Currency = currencyProvider
}