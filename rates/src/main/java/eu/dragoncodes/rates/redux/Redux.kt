package eu.dragoncodes.rates.redux

import android.text.Spannable
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import eu.dragoncodes.rates.data.Currency
import eu.dragoncodes.rates.data.Rate
import eu.dragoncodes.rates.network.RatesProvider
import eu.dragoncodes.rates.utils.roundToPlaces
import eu.dragoncodes.redux.base.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

data class RatesExchangeState(
    val isLoading: Boolean,
    val error: String? = null,
    val rate: Rate = Rate.EMPTY,
    val currencies: List<CurrencyState> = emptyList()
) : State {

    companion object {
        val Initial = RatesExchangeState(
            isLoading = true,
            error = null,
            rate = Rate.EMPTY,
            currencies = emptyList()
        )
    }
}

data class CurrencyState(
    val name: Currency,
    val quantity: Double,
    val formattedQuantity: CharSequence
) : State

sealed class CurrenciesActions : Action {

    object Init : CurrenciesActions()

    class RateLoaded(val rate: Rate) : CurrenciesActions()

    class BaseQuantityChange(val quantity: Double) : CurrenciesActions()

    class ErrorOccurred(val error: String) : CurrenciesActions()

    open class CurrencyRelated(val currency: Currency) : CurrenciesActions() {
        class BaseCurrencyChanged(currency: Currency) : CurrencyRelated(currency)
        class RequestRateUpdate(currency: Currency) : CurrencyRelated(currency)
    }
}

class RatesDownloadMiddleware(
    private val ratesProvider: RatesProvider,
    private val scope: CoroutineScope
) : Middleware<RatesExchangeState> {

    override fun invoke(store: Store<RatesExchangeState>, action: Action, next: DispatchFunction) {

        scope.launch {
            when (action) {
                is CurrenciesActions.CurrencyRelated -> {

                    val base = action.currency

                    try {
                        val rate = ratesProvider.fetch(base)
                            ?: return@launch next(CurrenciesActions.ErrorOccurred("Errors.couldNotFetch"))

                        next(CurrenciesActions.RateLoaded(rate))
                    } catch (e: Exception) {
                        next(CurrenciesActions.ErrorOccurred(e.message ?: "Errors.couldNotFetch"))

                        e.printStackTrace()
                    }
                }
            }
        }
    }
}

class RatesReducer : Reducer<RatesExchangeState> {
    override fun invoke(action: Action, old: RatesExchangeState): RatesExchangeState =
        when (action) {

            is CurrenciesActions.Init -> RatesExchangeState(isLoading = true)

            is CurrenciesActions.RateLoaded -> {

                val currentQuantity =
                    old.currencies.find { it.name == action.rate.base }?.quantity ?: 1.0

                invoke(
                    CurrenciesActions.BaseQuantityChange(currentQuantity),
                    old.copy(
                        isLoading = false,
                        rate = action.rate
                    )
                )
            }

            is CurrenciesActions.BaseQuantityChange -> {

                val rate = old.rate

                val quantity = action.quantity

                val newCurrencies = mutableListOf(
                    CurrencyState(
                        name = rate.base,
                        quantity = quantity,
                        formattedQuantity = "${quantity.roundToPlaces(2)}".wrapIntegers()
                    )
                )

                newCurrencies.addAll(rate.ratesForBase.map {
                    CurrencyState(
                        name = it.key,
                        quantity = quantity * it.value,
                        formattedQuantity = "${(quantity * it.value).roundToPlaces(2)}.".wrapIntegers()
                    )
                })

                old.copy(
                    currencies = newCurrencies
                )
            }

            else -> old
        }
}

private fun String.wrapIntegers(): CharSequence {
    val spannableString = SpannableString(this)

    // Assume dot is decimal point
    val dot = this.indexOf(".")

    if (dot != -1) {

        spannableString.setSpan(
            RelativeSizeSpan(1.1f),
            0,
            dot,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }

    return spannableString
}