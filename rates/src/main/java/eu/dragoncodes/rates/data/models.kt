package eu.dragoncodes.rates.data

import eu.dragoncodes.rates.network.RateDTO

typealias RatesForBase = Map<Currency, Double>

data class Rate(val base: Currency, val ratesForBase: RatesForBase) {
    companion object {
        val EMPTY = Rate("", emptyMap())
    }
}

typealias Currency = String

fun RateDTO.toRate() = Rate(this.base, this.rates)