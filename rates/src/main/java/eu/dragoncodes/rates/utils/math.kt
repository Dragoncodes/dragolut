package eu.dragoncodes.rates.utils

import kotlin.math.pow
import kotlin.math.round

fun Double.roundToPlaces(places: Int): Double {
    val pow = 10.0.pow(places.toDouble())

    return round(this * pow) / pow
}