package eu.dragoncodes.rates.utils

import android.content.res.Resources

val Int.dp
    get() = Resources.getSystem().displayMetrics.density * this