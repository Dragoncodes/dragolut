package eu.dragoncodes.rates.localization

import dagger.Component
import eu.dragoncodes.localization.LocalizationData
import javax.inject.Singleton

@Component(modules = [LocalizationModule::class])
interface LocalizationComponent {

    fun buildLocalizationData(): LocalizationData
}

fun String.localized() = DaggerLocalizationComponent.builder().build().buildLocalizationData()[this]