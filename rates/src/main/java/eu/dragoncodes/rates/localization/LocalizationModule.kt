package eu.dragoncodes.rates.localization

import dagger.Module
import dagger.Provides
import eu.dragoncodes.localization.Localization

@Module
class LocalizationModule {

    @Provides
    fun provideLanguage() = "en"

    @Provides
    fun provideLocalizationData(language: String) = Localization[language]
}
