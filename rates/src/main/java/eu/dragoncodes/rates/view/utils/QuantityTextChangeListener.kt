package eu.dragoncodes.rates.view.utils

import android.text.Editable
import android.text.TextWatcher

class QuantityTextChangeListener(
    private val quantityChangedCallback: (Double) -> Unit
) : TextWatcher {

    private var isEnabled = true

    override fun afterTextChanged(s: Editable?) {
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {

        text ?: return

        if (!isEnabled) return

        val value =
            try {
                if (text.isEmpty()) 0.0 else text.toString().toDouble()
            } catch (e: NumberFormatException) {
                0.0
            }

        quantityChangedCallback.invoke(value)
    }

    fun stopForAction(action: () -> Unit) {
        isEnabled = false

        action.invoke()

        isEnabled = true
    }
}