package eu.dragoncodes.rates.view.base

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.CallSuper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class ViewController(context: Context) : FrameLayout(context), CoroutineScope {

    abstract val layoutId: Int

    protected lateinit var contentView: View

    private lateinit var job: Job

    override val coroutineContext: CoroutineContext
        get() = job

    @CallSuper
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        job = Job()
        job.start()

        if (!::contentView.isInitialized) {
            contentView = LayoutInflater.from(context).inflate(layoutId, this, true)
        }
    }

    @CallSuper
    override fun onDetachedFromWindow() {

        job.cancel()

        super.onDetachedFromWindow()
    }
}