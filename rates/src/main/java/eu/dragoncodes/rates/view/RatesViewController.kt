package eu.dragoncodes.rates.view

import android.annotation.SuppressLint
import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.trading212.diverserecycleradapter.DiverseRecyclerAdapter
import com.trading212.diverserecycleradapter.layoutmanager.DiverseLinearLayoutManager
import eu.dragoncodes.rates.R
import eu.dragoncodes.rates.data.Currency
import eu.dragoncodes.rates.di.CurrenciesModule
import eu.dragoncodes.rates.di.DaggerRatesComponent
import eu.dragoncodes.rates.localization.localized
import eu.dragoncodes.rates.network.RatesUpdater
import eu.dragoncodes.rates.redux.CurrenciesActions
import eu.dragoncodes.rates.redux.RatesExchangeState
import eu.dragoncodes.rates.view.base.ViewController
import eu.dragoncodes.redux.base.Store
import eu.dragoncodes.redux.base.StoreSubscriber
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class RatesViewController(context: Context) : ViewController(context) {

    @Inject
    lateinit var store: Store<RatesExchangeState>

    @Inject
    lateinit var ratesUpdater: RatesUpdater

    private var baseCurrency: Currency = "BGN"

    private val adapter = DiverseRecyclerAdapter()

    private val storeSubscriber: StoreSubscriber<RatesExchangeState>

    private lateinit var recyclerView: RecyclerView

    private lateinit var ratesLabelTextView: TextView

    override val layoutId = R.layout.view_controller_rates

    init {
        DaggerRatesComponent
            .builder()
            .currenciesModule(CurrenciesModule { store.state.rate.base })
            .build()
            .inject(this)

        storeSubscriber = object : StoreSubscriber<RatesExchangeState> {

            override fun newState(state: RatesExchangeState) {
                launch(Dispatchers.Main) { refreshData(state) }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        recyclerView = findViewById(R.id.rvCurrencies)

        ratesLabelTextView = findViewById(R.id.tvRates)

        ratesLabelTextView.text = "Rates.label".localized()

        recyclerView.layoutManager = DiverseLinearLayoutManager(context)
        recyclerView.adapter = adapter


        store.add(storeSubscriber)

        store.dispatch(CurrenciesActions.Init)
        store.dispatch(CurrenciesActions.CurrencyRelated.RequestRateUpdate(baseCurrency))

        ratesUpdater.start(this) { store.dispatch(CurrenciesActions.RateLoaded(it)) }
    }

    override fun onDetachedFromWindow() {

        store.remove(storeSubscriber)

        ratesUpdater.stop()

        super.onDetachedFromWindow()
    }

    private fun refreshData(state: RatesExchangeState) {

        val newItems = state.toAdapterState()

        val diffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun getOldListSize() = adapter.itemCount

            override fun getNewListSize() = newItems.size

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return adapter.getItem<CurrencyRecyclerItem>(oldItemPosition).data.name == newItems[newItemPosition].data.name
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldCurrencyData = adapter.getItem<CurrencyRecyclerItem>(oldItemPosition).data
                val newCurrencyData = newItems[newItemPosition].data

                return oldCurrencyData.quantity == newCurrencyData.quantity
            }

            override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
                return newItems[newItemPosition].data
            }
        }, true)

        adapter.removeAll(false)
        adapter.addItems(newItems, false)

        diffResult.dispatchUpdatesTo(adapter)
    }

    private fun RatesExchangeState.toAdapterState() =

        currencies.map { currencyState ->
            CurrencyRecyclerItem.CurrencyData(
                name = currencyState.name,
                fullName = "Currency.${currencyState.name}.fullName".localized(),
                quantity = currencyState.quantity,
                formattedQuantity = currencyState.formattedQuantity,

                editTextGainedFocus = {
                    store.dispatch(CurrenciesActions.CurrencyRelated.BaseCurrencyChanged(it))
                },

                onQuantityChanged = { store.dispatch(CurrenciesActions.BaseQuantityChange(it)) }
            )
        }.map { CurrencyRecyclerItem((it)) }
}