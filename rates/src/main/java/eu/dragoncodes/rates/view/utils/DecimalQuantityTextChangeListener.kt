package eu.dragoncodes.rates.view.utils

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import java.lang.ref.WeakReference
import kotlin.math.max
import kotlin.math.min

class DecimalQuantityTextChangeListener(
    private val precision: Int,
    editText: EditText
) : TextWatcher {

    private val editTextRef = WeakReference<EditText>(editText)

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(input: Editable?) {

        val editText = editTextRef.get() ?: return

        input ?: return

        val inputString = input.toString()

        if (inputString.isEmpty()
            || precision == 0
            || !inputString.contains(".")
        ) {
            return
        }

        val decimalSeparator = "."

        if (decimalSeparator.startsWith(inputString)) {
            input.insert(0, "0")

            return
        }

        val dotIndex = inputString.indexOf(decimalSeparator)
        val decimalCharsCount = input.length - dotIndex

        if (decimalCharsCount > precision) {
            val cursorPosition = editText.selectionEnd

            input.replace(dotIndex + precision + 1, input.length, "")

            val newCursorPosition = min(max(0, cursorPosition), input.length)

            editText.setSelection(newCursorPosition)
        }
    }
}