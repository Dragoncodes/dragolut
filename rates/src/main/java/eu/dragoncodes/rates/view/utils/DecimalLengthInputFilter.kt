package eu.dragoncodes.rates.view.utils

import android.text.InputFilter
import android.text.Spanned

class DecimalLengthInputFilter(integerPartLength: Int, precision: Int) : InputFilter {

    private val decimalRegex = "-?\\d{0,$integerPartLength}(\\.\\d{0,$precision})?".toRegex()

    override fun filter(
        source: CharSequence?,
        start: Int,
        end: Int,
        dest: Spanned?,
        dstart: Int,
        dend: Int
    ): CharSequence {

        source ?: return ""

        // Do not filter text length if the source contains spaces, commas or hyphen, this means that the number is formatted
        if (source.toString().contains(" ") || source.toString().contains(",") || source.toString().contains(
                "-"
            )
        ) {
            return source
        }

        if (dest.isNullOrEmpty()) return source

        // Is is remove action replace else insert the new string
        val newInput = if (end == 0) {
            StringBuilder(dest).replace(dstart, dend, source.toString()).toString()
        } else {
            StringBuilder(dest).insert(dstart, source.toString()).toString()
        }

        val matcher = decimalRegex.matches(newInput)

        // If pattern doesn't match and user makes remove action return old char at this position.
        // If pattern doesn't match and user is adding symbols return empty string.
        // The goal is to imitate no changes if pattern doesn't match.
        if (!matcher) {
            return if (source == "") dest.toString().subSequence(dstart, dend) else ""
        }

        return source
    }
}