package eu.dragoncodes.rates.view

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.ViewCompat
import com.trading212.diverserecycleradapter.DiverseRecyclerAdapter
import eu.dragoncodes.rates.R
import eu.dragoncodes.rates.data.Currency
import eu.dragoncodes.rates.utils.dp
import eu.dragoncodes.rates.view.utils.DecimalLengthInputFilter
import eu.dragoncodes.rates.view.utils.DecimalQuantityTextChangeListener
import eu.dragoncodes.rates.view.utils.QuantityTextChangeListener
import java.util.*

class CurrencyRecyclerItem(override val data: CurrencyData) :
    DiverseRecyclerAdapter.RecyclerItem<CurrencyRecyclerItem.CurrencyData, CurrencyRecyclerItem.ViewHolder>() {

    override val type: Int = TYPE

    override fun createViewHolder(parent: ViewGroup, inflater: LayoutInflater): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.row_currency, parent, false))
    }

    class ViewHolder(itemView: View) : DiverseRecyclerAdapter.ViewHolder<CurrencyData>(itemView) {

        private val currencyIconImageView = findViewById<ImageView>(R.id.ivFlag)!!

        private val currencyNameTextView = findViewById<TextView>(R.id.tvName)!!

        private val currencyFullNameTextView = findViewById<TextView>(R.id.tvFullName)!!

        private val quantityEditText = findViewById<EditText>(R.id.etQuantity)!!

        private val quantityTextChangeListener: QuantityTextChangeListener =
            QuantityTextChangeListener { lastData?.onQuantityChanged?.invoke(it) }

        init {

            quantityEditText.apply {

                setOnFocusChangeListener { _, hasFocus ->

                    val lastData = lastData ?: return@setOnFocusChangeListener

                    if (hasFocus) {

                        lastData.editTextGainedFocus.invoke(lastData.name)
                    }
                }

                addTextChangedListener(
                    DecimalQuantityTextChangeListener(DECIMAL_PRECISION, quantityEditText)
                )
                addTextChangedListener(quantityTextChangeListener)

                filters =
                    arrayOf(
                        DecimalLengthInputFilter(
                            MAX_INT_DIGITS,
                            DECIMAL_PRECISION
                        )
                    )


                setOnEditorActionListener { _, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        return@setOnEditorActionListener true
                    }

                    return@setOnEditorActionListener false
                }

                hint = "0"
            }
        }

        override fun bindTo(data: CurrencyData?) {

            data ?: return

            currencyNameTextView.text = data.name
            currencyFullNameTextView.text = data.fullName

            handleCurrencyIcon(data)

            if (quantityEditText.isFocused) return

            val newText = data.formattedQuantity
            quantityTextChangeListener.stopForAction {
                quantityEditText.setText(
                    newText,
                    TextView.BufferType.SPANNABLE
                )
            }
        }

        override fun updateWith(data: CurrencyData?, update: Any) {
            super.updateWith(data, update)


        }

        private fun handleCurrencyIcon(data: CurrencyData) {
            val context = itemView.context

            val resources = context.resources

            val flagIconId = "ic_co_" + data.name.toLowerCase(Locale.ENGLISH)

            val flagDrawableId =
                resources.getIdentifier(flagIconId, "drawable", context.packageName)

            val drawable =
                try {
                    resources.getDrawable(flagDrawableId, context.theme)
                } catch (e: Resources.NotFoundException) {
                    null
                }

            if (drawable == null) {
                currencyIconImageView.visibility = View.INVISIBLE
            } else {
                currencyIconImageView.visibility = View.VISIBLE
            }

            currencyIconImageView.setImageDrawable(drawable)

            ViewCompat.setElevation(currencyIconImageView, 5.dp)
        }

        override fun unbind() {

            currencyIconImageView.setImageDrawable(null)

            super.unbind()
        }
    }

    class CurrencyData(
        val name: String,
        val fullName: String,
        val quantity: Double,
        val formattedQuantity: CharSequence,
        val editTextGainedFocus: (Currency) -> Unit,
        val onQuantityChanged: (Double) -> Unit
    )

    companion object {
        const val MAX_INT_DIGITS = 9

        const val DECIMAL_PRECISION = 2

        const val TYPE = 1337
    }
}