package eu.dragoncodes.rates

import eu.dragoncodes.rates.di.DaggerRatesComponent
import eu.dragoncodes.rates.redux.CurrenciesActions
import eu.dragoncodes.rates.redux.RatesExchangeState
import eu.dragoncodes.redux.base.StoreSubscriber
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.runBlocking
import org.junit.Test

class MainTestSuite {

    @Test
    fun mainTest() = runBlocking {

        val ratesProvider = DaggerRatesComponent.builder().build().buildRatesProvider()

        val fetchResult = ratesProvider.fetch("EUR")

        fetchResult?.ratesForBase?.map {
            println("\"Currency.${it.key}.fullName\" to \"\",")
        }

        assert(fetchResult?.base == "EUR")
    }

    @Test
    fun reduxTest() = runBlocking {

        val stateChannel = Channel<RatesExchangeState>(Channel.BUFFERED)

        val sub = object : StoreSubscriber<RatesExchangeState> {
            override fun newState(state: RatesExchangeState) {
                stateChannel.sendBlocking(state)
            }
        }

        val store = DaggerRatesComponent.builder().build().buildRatesStore()

        store.add(sub)

        store.dispatch(CurrenciesActions.CurrencyRelated.RequestRateUpdate("EUR"))

        for (state in stateChannel) {

            if (!state.isLoading) {
                assert(state.currencies.isNotEmpty())

                break
            }
        }

        Unit
    }
}