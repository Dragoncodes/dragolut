package eu.dragoncodes.localization

import org.junit.Test

class MainTestSuite {

    @Test
    fun testLocalization() {
        val correct = "No connection"

        assert(Localization["en"]["Errors.connection"] == correct)
    }

    @Test
    fun testMissingKey() {
        try {
            Localization["en"]["No such key"]

            assert(false)
        } catch (e: IllegalStateException) {
            assert(true)
        }
    }

    @Test
    fun testMissingLanguage() {
        try {
            Localization["ar"]

            assert(false)
        } catch (e: IllegalStateException) {
            assert(true)
        }
    }
}