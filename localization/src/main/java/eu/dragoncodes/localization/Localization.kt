package eu.dragoncodes.localization

object Localization {
    private val languageStore = mapOf(
        "en" to en()
    )

    operator fun get(language: String) = languageStore[language]
        ?: throw IllegalStateException("Missing definitions for language: $language")
}

private fun en() = localizationData {

    localizationParts(
        "Rates.label" to "Rates",

        "Errors.server" to "Connection error",

        "Errors.connection" to "No connection",

        *generateCurrenciesMetadata()
    )
}