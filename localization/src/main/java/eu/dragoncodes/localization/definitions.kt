package eu.dragoncodes.localization

class LocalizationData {

    lateinit var store: Map<String, String>

    operator fun get(key: String) =
        store[key] ?: throw IllegalStateException("Missing translation for key $key")

    fun localizationParts(vararg values: Pair<String, String>) {
        store = mapOf(*values)
    }
}

inline fun localizationData(builder: LocalizationData.() -> Unit): LocalizationData {
    val localizationData = LocalizationData()
    builder.invoke(localizationData)
    return localizationData
}